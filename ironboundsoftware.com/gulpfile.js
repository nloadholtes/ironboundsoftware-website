var gulp = require("gulp");
//var browsersync = require("browser-sync").create()
var handlebars = require("gulp-compile-handlebars");
var rename = require("gulp-rename");
var cssnano = require("gulp-cssnano");

gulp.task("cssnano", gulp.series(function(done){
    gulp.src(["src/styles/**/*.css"])
        .pipe(cssnano())
        .pipe(gulp.dest("./output/static"));
  //      .pipe(browsersync.stream());
	done();
}));

gulp.task("templates", gulp.series(function(done) {
    var data = {};
    var options = {
        batch: ["src/templates/partials"]
    }
    return gulp.src(["src/templates/**/*.hbs", "!src/templates/partials/**/*.hbs"])
        .pipe(handlebars(data, options))
        .pipe(rename(function(path){
            path.extname = ".html"
        }))
        .pipe(gulp.dest("./output"));
    //    .pipe(browsersync.stream());
	done();
}));

gulp.task("images", gulp.series(function(done) {
    gulp.src(["src/img/**/*.*"])
        .pipe(gulp.dest("./output/static"));
	done();
}));

gulp.task('default', gulp.series("cssnano", "templates", "images"), function(){
    console.log("Starting gulp");
  //  browsersync.init({
  //      server: "./output"
  //  });
    gulp.watch("src/styles/**.*.css", ["cssnano"]);
    gulp.watch("src/templates/**/*.hbs", ["templates"]);
   // gulp.watch("output/*.html", browsersync.reload)
});
