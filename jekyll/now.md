---
layout: default
title: Iron Bound Software - Now
me_links: true
---

## Current project(s)

_Last updated: 2025-02-12_

### Cloud Drive Cleaner
_Coming soon!_ A utility to help you find duplicate files in your Google Cloud Drive. Instead of buying more space to store copies of things, why not just get rid the duplicates? Get a free scan to see if you have space that could be freed up!

[Launching soon!](https://cloud-drive-cleaner.ironboundsoftware.com)

Tech stack: Python Flask, OAuth, Google cloud, Docker

### Addit Pulse
_Decommissioning_ [Addit Pulse](https://additpulse.com) is an experiment in competitive intelligence. Plug in a subreddit your target audience is in, and start seeing who is advertising there.

This project, while cool (to me at least), has not gotten very good traction. I've turned off the ingestion portion so no new data is being collected. The existing data is still searchable, so try it out!

Tech stack: Python Flask, puppeteer, Claude, Docker, sqlite 
