---
layout: default
title: Home
hero:
  title: Building Software That Matters
  description: With over 15 years of experience crafting high-quality software solutions, I specialize in Python development, automation, and developer tools.
show_contact: true
---

{% include projects-section.html %}
