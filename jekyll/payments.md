---
layout: default
title: Iron Bound Software - Payment
me_links: true
---

  <style>
      .payment-container {
          background: #ffffff;
          border-radius: 10px;
          padding: 30px;
          box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
      }

      h1 {
          color: #2d3748;
          font-size: 24px;
          margin-bottom: 20px;
          font-weight: 600;
      }

      .description {
          color: #4a5568;
          margin-bottom: 25px;
      }

      #payment-form {
          display: flex;
          flex-direction: column;
          gap: 20px;
      }

      .input-group {
          display: flex;
          flex-direction: column;
          gap: 8px;
      }

      .input-group label {
          font-weight: 500;
          color: #4a5568;
      }

      .currency-input {
          position: relative;
          display: flex;
          align-items: center;
      }

      .currency-symbol {
          position: absolute;
          left: 12px;
          color: #4a5568;
          font-weight: 500;
      }

      #amount {
          width: 100%;
          padding: 12px;
          padding-left: 30px;
          border: 2px solid #e2e8f0;
          border-radius: 6px;
          font-size: 16px;
          outline: none;
          transition: border-color 0.2s ease;
      }

      #amount:focus {
          border-color: #4299e1;
      }

      button {
          background-color: #4299e1;
          color: white;
          padding: 12px 24px;
          border: none;
          border-radius: 6px;
          font-size: 16px;
          font-weight: 500;
          cursor: pointer;
          transition: background-color 0.2s ease;
      }

      button:hover {
          background-color: #3182ce;
      }

      button:active {
          transform: translateY(1px);
      }
  </style>

<!-- payment.html -->
<div class="payment-container">
      <h1>Make a Payment</h1>
      <p class="description">
          If you would like to make a payment, please enter the amount below. You'll be redirected to our secure payment processor to complete the transaction.
      </p>
      <form id="payment-form">
          <div class="input-group">
              <label for="amount">Payment Amount</label>
              <div class="currency-input">
                  <span class="currency-symbol">$</span>
                  <input 
                      type="number" 
                      id="amount" 
                      min="1" 
                      step="0.01" 
                      placeholder="0.00"
                      required
                  >
              </div>
          </div>
          <button type="submit">Continue to Payment</button>
      </form>
  </div>

<script>
const STRIPE_PAYMENT_LINK = 'https://buy.stripe.com/4gw4kh44IcXA4W43cc';

document.getElementById('payment-form').addEventListener('submit', (e) => {
  e.preventDefault();
  const amount = document.getElementById('amount').value;
  // Stripe payment links support amount override via URL parameter
  window.location.href = `${STRIPE_PAYMENT_LINK}?amount=${amount * 100}`; // Convert to cents
});
</script>


_Thank you!_
