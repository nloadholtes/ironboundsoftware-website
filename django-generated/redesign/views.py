# Create your views here.
from django.template import loader, Context
from django.http import HttpResponse
from django.shortcuts import render_to_response

def index(req):
	return render_to_response('index.html',)

def consulting(req):
	return render_to_response('consulting.html',)

def products(req):
	return render_to_response('products.html',)

def passwordmaker(req):
	return render_to_response('passwordmaker.html',)

def sn(req):
	return render_to_response('sn.html',)

def sudokuhelper(req):
	return render_to_response('sudokuhelper.html',)
