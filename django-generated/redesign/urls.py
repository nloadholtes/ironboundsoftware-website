from django.conf.urls.defaults import *
#from redesign.views import *

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^ironboundsoftware/', include('ironboundsoftware.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs'
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # (r'^admin/(.*)', admin.site.root),
	url(r'^index', 'redesign.views.index'),
	url(r'^$', 'redesign.views.index'),
	url(r'^consulting', 'redesign.views.consulting'),
	url(r'^released_products', 'redesign.views.products'),
	url(r'^passwordmaker', 'redesign.views.passwordmaker'),
	url(r'^sn', 'redesign.views.sn'),
	url(r'^products/SudokuHelper', 'redesign.views.sudokuhelper'),
)
